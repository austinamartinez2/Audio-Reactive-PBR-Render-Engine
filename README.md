# Audio Reactive PBR Render Engine


### Dependancies
* [OpenGl](https://www.opengl.org/)
* [GLAD](https://glad.dav1d.de/)
* [GLFW](https://www.glfw.org/)
* [stb_image](https://github.com/nothings/stb/blob/master/stb_image.h)
* [GLM](https://glm.g-truc.net/0.9.9/index.html)
* [Assimp](http://www.assimp.org/)
* [BASS](https://www.un4seen.com/)
